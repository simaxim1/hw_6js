/* 
    1. Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування.
        Це спосіб відображення деяких символів у вигляді тексту, а не частини коду. 
        Приклад: console.log('I'm live') - помилка у закінченні строки.
                 console.log('I\'m live') - рішення.

    2. Які засоби оголошення функцій ви знаєте?
        - декларування - function Name() {};
        - експресія - const x = function() {};
        - стрілочна функція - const x = (param) => {};
        - метод для властивості об'єкта;

    3. Що таке hoisting, як він працює для змінних та функцій?
        Всі створені функції можливо розташувати внизу, а виконання робити в іньшому порядку (до її об'явлення).
        Приклад:

        userName("John");
        
        function userName(name) {
            console.log("userName is " + name);
            
*/
           
'use strict' 

const createNewUser = () => { 

    const firstName = prompt('Add your name');
    const lastName  = prompt('Add your Last Name'); 

    let user = {}; 

    Object.defineProperties(user, {
        firstName: {
            value: firstName,
            writable: false, 

        },
          
        lastName: {
            value: lastName,
            writable: false, 

        }, 

        getLogin:  {
            value: function() {
            return this.firstName.toLowerCase()[0] + this.lastName.toLowerCase();
            
            },
        },
        
        birthday: {
            value: prompt('Add Your birthday','dd.mm.yyyy'),

        },
        
        getAge: {
            value: function() {
                const date = this.birthday;
                const newDate = Date.parse(`${date.slice(6,10)}-${date.slice(3,5)}-${date.slice(0,2)}`);
                const age = parseInt((Date.now() - newDate)/365/24/60/60/1000);
                return age;
            },
        },
          
        getPassword: {
            value: function() {
                const password = `${this.firstName.toUpperCase()[0]}${this.lastName.toLowerCase()}${this.birthday.slice(6,10)}`; 
                return password;

            },
        },
          
    });

    return user;

};

const newUser = createNewUser(); 

console.log(`Your login: ${newUser.getLogin()}`); 
console.log(`Your age: ${newUser.getAge()}`);
console.log(`Your password: ${newUser.getPassword()}`);